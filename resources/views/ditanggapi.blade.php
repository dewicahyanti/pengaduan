@extends("layouts.app")
@section("title", "Ditanggapi")
@section("content")
<div class="container-fluid">
                        <br>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                            <li class="breadcrumb-item active">Pengaduan</li>
                            <li class="breadcrumb-item active">Ditanggapi</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Laporan 
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Pengadu</th>
                                                <th>Jenis Aduan</th>
                                                <th>Aduan</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach $laporans as $item
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->nama_pengadu }}</td>
                                                <td>{{ $item->jenis_aduan }}</td>
                                                <td>{{ $item->aduan }}</td>
                                                <td></td>
                                                <td>
                                                <a href="{{ url('delete-tanggapan', $item->$id)}}" class="btn btn-outline-danger">Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection