@extends("layouts.app")
@section("title", "add-laporan")
@section("content")
<div class="container-fluid">
                        <br>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">Laporan</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                            Laporan
                            </div>
                            <div class="card-body">
                                <form action="{{ url('save-laporan') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Nama Pengadu</label>
                                        <input type="text" name="nama_pengadu" id="nama_pengadu"
                                            class="form-control" placeholder="Nama Pengadu" value="{{$tambahlaporan->nama_pengadu}}" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Jenis Aduan</label>
                                        <select name="jenis_aduan" id="jenis_aduan" aria-placeholder="jenis_aduan" class="form-control">
                                            <option value="Infrastruktur">Infrastruktur</option>
                                            <option value="Kebersihan">Kebersihan</option>
                                            <option value="Keamanan">Keamanan</option>
                                            <option value="Kenyamanan">Kenyamanan</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Aduan</label>
                                        <input type="text" name="aduan" id="aduan"
                                            class="form-control" placeholder="Aduan" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Lapor</button>
                                        <a href="/laporan" class="btn btn-secondary" style="margin-right: 1rem">Kembali</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
@endsection