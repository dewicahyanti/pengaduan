@extends("layouts.app")
@section("title", "add-pengadu")
@section("content")
<div class="container-fluid">
                        <br>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">Data Pengadu</li>
                        </ol>
                            <div class="card-body">
                                <form action="{{ url('save-pengadu') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Nama Pengadu</label>
                                        <input type="text" name="nama_pengadu" id="nama_pengadu"
                                            class="form-control" placeholder="Nama Pengadu" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                        <select name="jenis_kelamin" id="jenis_kelamin" aria-placeholder="Jenis Kelamin" class="form-control">
                                            <option selected>Jenis Kelamin</option>
                                            <option value="Laki-laki">Laki-laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">No. Hp</label>
                                        <input type="text" name="no_hp" id="no_hp"
                                            class="form-control" placeholder="No. Hp" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Alamat</label>
                                        <input type="text" name="alamat" id="alamat"
                                            class="form-control" placeholder="Alamat" required>
                                    <br>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Tambah</button>
                                        <a href="/datapengadu" class="btn btn-secondary" style="margin-right: 1rem">Kembali</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
@endsection