@extends("layouts.app")
@section("title", "Tanggapan")
@section("content")
<div class="container-fluid">
                        <br>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">Data Pengadu</li>
                        </ol>
                            <div class="card-body">
                                <form action="{{ url('save-tanggapan') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                    <label for="exampleFormControlInput1">Tanggapan :</label>
                                    <textarea name="tanggapan" rows="5" cols="40"></textarea>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Tambah</button>
                                        <a href="/datapengadu" class="btn btn-secondary" style="margin-right: 1rem">Kembali</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
@endsection