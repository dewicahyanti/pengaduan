@extends("layouts.app")
@section("title", "Data Pengadu")
@section("content")
    <div class="container-fluid">
        <br>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item active">Master Data</li>
            <li class="breadcrumb-item active">Data Pengadu</li>
        </ol>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                <a href="{{ route('add-pengadu') }}" class="btn btn-primary" id="createPengadu"
                    style="margin-left: 1rem">
                    Tambahkan Pengadu
                </a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pengadu</th>
                                <th>Jenis Kelamin</th>
                                <th>No. Hp</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datapengadus as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nama_pengadu }}</td>
                                <td>{{ $item->jenis_kelamin }}</td>
                                <td>{{ $item->no_hp }}</td>
                                <td>{{ $item->alamat }}</td>
                                <td>
                                    <a href="{{ url('edit-pengadu', $item->id) }}" class="btn btn-outline-secondary">Edit</a>
                                    <a href="{{ url('delete-pengadu', $item->id) }}" class="btn btn-outline-danger">Hapus</a>
                                    <a href="{{ url('add-laporan', $item->id) }}" class="btn btn-outline-secondary">Lapor</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection