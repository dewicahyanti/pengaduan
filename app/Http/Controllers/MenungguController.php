<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Laporan};

class MenungguController extends Controller
{
    //
    public function index() 
    {
        $laporans = Laporan::all();
        return view('menunggu', compact('laporans'));
    }
}
