<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model
{
    public $timestamps = false;

    protected $table = "pengguna";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'nama',
        'email',
        'password',
        'level',
    ];
}
