<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisAduan extends Model
{
    public $timestamps = false;

    protected $table = "jenis";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'nama_pengadu',
        'jenis_aduan',
        'aduan',
    ];
}
