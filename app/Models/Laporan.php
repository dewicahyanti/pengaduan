<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    public $timestamps = false;

    protected $table = "laporan";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'nama_pengadu',
        'jenis_aduan',
        'aduan',
    ];
}
