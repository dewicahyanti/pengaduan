<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    DashboardController,
    penggunaController,
    datapengaduController,
    laporanController,
    jenisaduanController,
    AuthController,
    tanggapanController,
    MenungguController
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function() {
//     return "Home";
// });

Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login'])->name('login.post');
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth'], function () {

Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
});

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/pengguna', [penggunaController::class, 'index']);
Route::get('add-pengguna', [penggunaController::class, 'create'])->name('add-pengguna');
Route::post('save-pengguna', [penggunaController::class, 'store'])->name('save-pengguna');
Route::get('edit-pengguna/{id}', [penggunaController::class, 'edit'])->name('edit-pengguna');
Route::post('update-pengguna/{id}', [penggunaController::class, 'update'])->name('update-pengguna');
Route::get('delete-pengguna/{id}', [penggunaController::class, 'destroy'])->name('delete-pengguna');



Route::get('/laporan', [laporanController::class, 'index']);
Route::get('/add-laporan/{id}', [laporanController::class, 'create'])->name('add-laporan');
Route::post('save-laporan', [laporanController::class, 'store'])->name('save-laporan');
Route::get('delete-laporan/{id}', [laporanController::class, 'destroy'])->name('delete-laporan');

Route::get('/datapengadu', [datapengaduController::class, 'index']);
Route::get('add-pengadu', [datapengaduController::class, 'create'])->name('add-pengadu');
Route::post('save-pengadu', [datapengaduController::class, 'store'])->name('save-pengadu');
Route::get('edit-pengadu/{id}', [datapengaduController::class, 'edit'])->name('edit-pengadu');
Route::post('update-pengadu/{id}', [datapengaduController::class, 'update'])->name('update-pengadu');
Route::get('delete-pengadu/{id}', [datapengaduController::class, 'destroy'])->name('delete-pengadu');

Route::get('/tanggapan', [tanggapanController::class, 'index']);
Route::get('add-tanggapan', [tanggapanController::class, 'create'])->name('add-tanggapan'); 

Route::get('/menunggu', [MenungguController::class, 'index']);

Route::get('/ditanggapi', function() {
    return view('ditanggapi');
});

Route::get('/selesai', function() {
    return view('selesai');
});



// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');